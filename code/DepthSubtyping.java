import java.util.List;
import java.util.ArrayList;

public class DepthSubtyping {

  public static void main(String[] arg) {
    Dog d1 = new Dog();
    Dog d2 = new Dog();
    Dog[] dogs = {d1, d2};
    Animal[] animals = dogs;
 //   animals[1] = new Animal();
    dogs[1].bark();

  }
}

class Animal {

}

class Dog extends Animal {
  public void bark() { System.out.println("bow wow!"); }
}
