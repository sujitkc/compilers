\documentclass{article}
\usepackage{xcolor}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{listings}
\usepackage{hyperref}
\usepackage{alltt}
\usepackage{mathpartir}
\usepackage{framed}
\usepackage{tikz}
\usetikzlibrary{automata,positioning,shapes,arrows,backgrounds,fit}

\title{Implementation of Lexical Analyser}
\author{Sujit Kumar Chakrabarti}
\date{}
\begin{document}
\maketitle

Compiler texts usually provide a detailed treatment to the theoretical aspects of the design of a lexical analyser. In most cases, the issues of putting together all these concepts in a real working lexical analyser are typically never discussed. Probably, there's good reason for this too, as very good tools exist that automatically generate lexical analysers from lexical specifications provided as regular expression patterns.

Yet, manually implementing a lexical anlyser that embodies all the concepts taught in a compilers text or course is an interesting engineering exercise. Doing this seems to provide some nuanced insights about how such a system can be designed. Experience thus earned is likely to be useful in solving other software design problems sharing technical properties with the lexical analyser.

In this article, we show how a lexical analyser can be implemented using C++. We do so using an example discussed in the following section.

\section{Example}
Here, we design a lexical analyser that recognises the follow token classes:
\begin{enumerate}
	\item \textbf{Assignment.} (\texttt{ASSIGN}) Represented by the single character `='.
	\begin{quote}
		\textit{Regular expression:} =
		
		\textit{Automaton:}
\begin{center}
	\resizebox{0.3\textwidth}{!}{%
		\begin{tikzpicture}[auto,
		->,
		>=stealth
		]
		\node[initial, state]   (0)                       {$0$};
		\node[state, accepting] (1) [right = of 0]       {$1$};
		
		\path (0) edge node {$=$} (1)
		;
		
		\end{tikzpicture}
	}
\end{center}
		
	\end{quote}
	\item \textbf{Equals.} (\texttt{EQ}) Represented by the string `=='.
	\begin{quote}
		\textit{Regular expression:} ==

		\textit{Automaton:}
\begin{center}
	\resizebox{0.5\textwidth}{!}{%
		\begin{tikzpicture}[auto,
		->,
		>=stealth
		]
		\node[initial, state]   (0)                      {$0$};
		\node[state, accepting] (1) [right = of 0]       {$1$};
		\node[state, accepting] (2) [right = of 1]       {$2$};
		
		\path (0) edge node {$=$} (1)
		      (1) edge node {$=$} (2)
		;
		
		\end{tikzpicture}
	}
\end{center}

	\end{quote}
	\item \textbf{Identifiers.} (\texttt{ID}) Represented by any string that starts with an alphabetic character and is followed by zero or more alphanumeric characters.
	\begin{quote}
		\textit{Regular expression:} $\alpha[\alpha | N]\star$
		
\textit{Automaton:}
\begin{center}
	\resizebox{0.5\textwidth}{!}{%
		\begin{tikzpicture}[auto,
		->,
		>=stealth
		]
		\node[initial, state]   (0)                       {$0$};
		\node[state, accepting] (1) [right = of 0]       {$1$};
		
		\draw[->] (0) to node {$\alpha$} (1);
		\draw[->, loop right] (1) to node {$\alpha, N$} (1);
		
		\end{tikzpicture}
	}
\end{center}
	\end{quote}

	\item \textbf{Whitespace.} (\texttt{WS}) Represented by a non-empty string of spaces or tabs.
	\begin{quote}
		\textit{Regular expression:} $[SPACE | TAB]+$
		
\textit{Automaton:}
\begin{center}
	\resizebox{0.7\textwidth}{!}{%
		\begin{tikzpicture}[auto,
		->,
		>=stealth
		]
		\node[initial, state]   (0)                       {$0$};
		\node[state, accepting] (1) [right = 2.5cm of 0]       {$1$};
		
		\draw[->] (0) to node {$SPACE, TAB$} (1);
		\draw[->, loop right] (1) to node {$SPACE, TAB$} (1);
		
		\end{tikzpicture}
	}
\end{center}
	\end{quote}
\end{enumerate}

A practical recogniser would try to consume as much of the input string as possible returning a token. For instance, if the input in \texttt{"ab"}, and the input pointer has just consume \texttt{'a'}, it has a choice of returning the \texttt{ID} token with lexeme \texttt{"a"}. However, the lexer must hang on before hurrying to do that. It must look ahead of where it currently is and decide on its next move based on what it sees. Here, for example, it would see that the next character to be consumed is \texttt{'b'}. This, if eaten, would help construct a longer lexeme, and result in further progress of the lexical analysis process before a token is returned. Hence, the lexical analyser, instead of returning rightaway will go ahead and consume \texttt{'b'}. Thereafter, it will find that either the input string is over, or the next unconsumed chanracter is a non-alphanumeric one. Thus, the lexical analyser will return \texttt{ID} token class \texttt{"ab"} being the lexeme.

\section{Combining the Automata}
There are multiple approaches possible in designing the combined recogniser. The conceptually simplest is to merge all the automata corresponding to each token class into one. This is the approach we take here. The combined automaton for the token classes is as shown in figure~\ref{f:dfa}:

\begin{figure}[H]
\begin{center}
	\resizebox{0.7\textwidth}{!}{%
		\begin{tikzpicture}[auto,
		->,
		>=stealth
		]
		\node[initial, state]   (0)                      {$0$};
		\node[state, accepting] (1) [right = of 0]       {$1$};
		\node[state, accepting] (2) [right = of 1]       {$2$};		
		\node[state, accepting] (3) [below = of 1]       {$3$};
		\node[state, accepting] (4) [below = of 3]       {$4$};
		
		\draw[->] (0) to node {$=$} (1);
		\draw[->] (1) to node {$=$} (2);
		\draw[->] (0) to node {$\alpha$} (3);
		\draw[->] (0) to node[left]{SPACE, TAB} (4);
		\draw[->, loop right] (3) to node {$\alpha,N$} (3);
		\draw[->, loop right] (4) to node[below=0.5cm]{SPACE, TAB} (4);
		
		\end{tikzpicture}
	}
\end{center}

\caption{The Combined Automaton}
\label{f:dfa}
\end{figure}

\section{Recognising the Next Token}
The central function in lexical analysis is the stateful procedure that begins consuming character upon character until it can consume no other. At that point it has to make a decision as to whether it has been able to recognise a valid token. If yes, that token is returned; otherwise, an error is announced. So, at the high level, the algorithm goes like this:

\begin{algorithm}
\begin{algorithmic}[0]
	\Procedure{nextToken}{}
	\State $s \gets D.s_0$
	\While{there is input left and move is possible with the next character}
		\State Keep consuming the characters and moving the automaton
	\EndWhile
	\If{current state is an accepting state}
		\State \textbf{return} the corresponding token
	\Else
	\State \textbf{raise} lexical error
	\EndIf
	\EndProcedure
	
\end{algorithmic}
\caption{\lstinline@next_token@ algorithm}	
\end{algorithm}

\section{Architecture and Implementation}
Now, let's take a look at the implementation that we have done. The system has been designed within a \lstinline@Scanner@ class, which encapsulates an instance of \lstinline@DFA@ class.

\subsection{Class \lstinline@DFA@}
\lstinline@DFA@ class provides the basic API for creating a deterministic finite state automaton. An instance is created by specifying the list of state IDs (which are integers), the initial state ID, and the list of final states ID. The transitions are later added to the DFA using the \lstinline@add_transition@ method in a very user-friendly manner. Internally, the DFA has been designed like a directed graph using adjacency list style connections.

\subsection{Class \lstinline@Scanner@}
The \lstinline@Scanner@ class implements the functionalities of the lexical analyser. It keeps account of the input string, and the progress of the lexical analysis through this string. It actuates the DFA by feeding it one character at a time, and makes the decision regarding acceptance or rejection of the input string at appropriate times. the \lstinline@next_token@ method returns the next token beginning at the current position on the input string. \lstinline@scan@ method returns all the tokens contained in the input string, by making repeated calls to \lstinline@next_token@.

\begin{figure}
\begin{center}
	\resizebox{0.7\textwidth}{!}{%
		\begin{tikzpicture}[auto,
		->,
		>=stealth,
		%  node distance=1cm,
		inp/.style={%
			rectangle, draw=black, fill=gray!10, very thick,
			text ragged, minimum height=2em, align=center
		}
		]


		\node[initial, state] at (0,0)  (0)                       {$0$};
		\node[state, accepting] (1) [right = 2.5cm of 0]       {$1$};

		\draw[->] (0) to node {SPACE, TAB} (1);
		\draw[->, loop right] (1) to node[name = t] {SPACE, TAB} (1);

		\begin{pgfonlayer}{background}
		\node[fit=(0)(1)(t), rectangle, draw=black, fill=gray!10, label=above:\textbf{DFA}] (dfa) {};
		\end{pgfonlayer}
		
		\begin{scope}[node distance = 0cm and 0cm]
			\node[inp, above=6cm of dfa.south east, xshift=-3cm] (i0)                      {h};
			\node[inp] (i1) [right = of i0]      {e};
			\node[inp] (i2) [right = of i1]      {l};
			\node[inp] (i3) [right = of i2]      {l};
			\node[inp] (i4) [right = of i3]      {o};
			\node[left = 2cm of i0](inp) {input};
			\node[below = 0.5cm of inp](pos) {position};
			\node[below = 1cm of pos](toklbl) {tokens};
			\node[right =1.5cm of toklbl](tok) {
				\begin{tabular}{| l | l |}
				\hline
				\textbf{State} & \textbf{Token} \\
				\hline
				1 & ASSIGN \\
				2 & EQ     \\
				3 & ID     \\
				4 & WS     \\
				\hline
				\end{tabular}	
			};				
			\draw[dashed] (inp) -- (i0);
			\draw[dashed, bend right] (pos) to (i2.south);
		\end{scope}

		\begin{pgfonlayer}{background}
		\node[fit=(dfa)(i0)(i4), rectangle, draw=black, very thick, fill=none, label=above:\textbf{Scanner}] (scanner) {};
		\end{pgfonlayer}
		
	\end{tikzpicture}
	}
\end{center}
\caption{The Lexical Analyser Architecture}
\end{figure}

\section{User Instructions}
The executable \texttt{la} is created by running \texttt{make}. \texttt{la} can be run simply without provided any command line arguments.

\subsection{Navigating the Code}
The \lstinline@main@ function is in the file \texttt{la.cpp}. The above example is implemented in the test case function \lstinline@t3@. In \lstinline@t3@, we create an object \lstinline@dfa@ of type \lstinline@DFA@. We construct it to resemble the DFA shown in figure~\ref{f:dfa}.
\end{document}