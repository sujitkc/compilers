#include <iostream>
#include <string>
#include <vector>
#include <map>

#include "dfa.h"

using namespace std;

State::State(int _id) : id(_id) {}
State * State::find_next_state(char inp) {
  if(successors.count(inp) == 0) { 
    return NULL;
  }
  return successors[inp];
}

State::~State() {
  cout << "Deleting state " << id << endl;
}

// assumes: it has been ensured by the caller that successors[inp] doesn't exist.
// Otherwie, may lead to overwriting error.
void State::add_transition(char inp, State *d) {
  successors[inp] = d;
}

string  State::to_string() {
  string s = "\nstate (id = ";
  s += std::to_string(id);
  s += " transitions = {";
  for(map<char, State *>::iterator it = successors.begin(); it != successors.end(); ++it) {
    s += "(" + std::to_string(id) + ", " + it->first + ", " + std::to_string(it->second->id) + "); ";
  }
  s += "})\n";
  return s;
}

DFA::DFA() {}

DFA::DFA(vector<char> alpha, vector<int> _states, int init_state, vector<int> fs) {
  current_state = NULL;

  // storing the alphabet in a map for faster search.
  for(unsigned int i = 0; i < alpha.size(); i++) {
    alphabet[alpha[i]] = i;
  }
  
  // Adding all states
  for(unsigned int i = 0; i < _states.size(); i++) {
    add_state(_states[i]);
  }

  // Adding initial state
  State *init = find_state(init_state);
  if(init == NULL) {
    throw ("Invalid initial state " + std::to_string(init_state));
  }
  current_state = init;
  initial_state = init;

  // Adding final states
  for(unsigned int i = 0; i < fs.size(); i++) {
    State *st = find_state(fs[i]);
    if(st == NULL) {
      throw ("Invalid final state " + std::to_string(fs[i]));
    }
    final_states.push_back(st);
  }
}

DFA::~DFA() {
  cout << "Deleting DFA with " << states.size() << " states ..." << endl;
  for(unsigned int i = 0; i < states.size(); i++) {
    delete states[i];
  }
}
/*
  Given an int n standing for the id of a state, return the pointer to the state
  which has the same ID. If such a state is not found, return NULL.
*/
State * DFA::find_state(int n) {
  for(unsigned int i = 0; i < states.size(); i++) {
    State *st = states[i];
    if(st->id == n) {
      return st;
    }
  }
  return NULL;
}

/*
  Given an int n standing for the ID of a state to be added, check if a state with
  the given ID exists. If yes, raise an exception. Else, create and add the new
  state.
*/
void DFA::add_state(int n) {
  if(find_state(n) != NULL) {
    throw "Add state failed: state " + std::to_string(n) + " already exists.";
  }
  State *st = new State(n);
  states.push_back(st);
}

/*
  Given int s, char inp and int d, check if a state src with ID s, and a state dst with
  ID d exists. If not create new ones. Then add dst as a successor to src on input inp.
*/
void DFA::add_transition(int s, char inp, int d) {
  State *src = find_state(s);
  if(src == NULL) {
    src = new State(s);
  }
  State *dst1 = src->find_next_state(inp);
  if(dst1 != NULL) {
    throw "Add transition failed : Transition (" + std::to_string(s) + ", " +
      std::to_string(inp) + ", " + std::to_string(d) + ") already exists.";
  }
  State *dst = find_state(d);
  if(dst == NULL) {
    dst = new State(d);
  }
  src->add_transition(inp, dst);
}

void DFA::move(char c) {
  current_state = current_state->find_next_state(c);
}

bool DFA::is_accepted() {
  for(unsigned int i = 0; i < final_states.size(); i++) {
    if(current_state == final_states[i]) {
      return true;
    }
  }
  return false;
}

void DFA::reset() {
  current_state = initial_state;
}

State *DFA::get_current_state() {
  return current_state;
}

string DFA::to_string() {
  string s = "";
  s += "states = {";
  for(unsigned int i = 0; i < states.size(); i++) {
    s += states[i]->to_string() + ", ";
  }
  s += "}\n";

  s += "alphabet = {";
  for(map<char, unsigned int>::iterator it = alphabet.begin(); it != alphabet.end(); ++it) {
    s += string(1, it->first)  + ", ";
  }
  s += "}\n";

  return s;
}
