#ifndef DFA_H
#define DFA_H

#include <iostream>
#include <string>
#include <vector>
#include <map>

class State;

using namespace std;

class State {
  public:
    const int id;
    map<char, State *> successors;

    State(int _id);
    ~State();
    void add_transition(char, State *);
    State *find_next_state(char inp);
    string to_string();
};

class DFA {
  private:
    State *current_state;
    State *initial_state;
    vector<State *> states;
    map<char, unsigned int> alphabet;
    vector<State *> final_states;

  private:
    string get_string(char c);
  public:
    DFA();
    DFA(vector<char> alpha, vector<int> _states, int init_state, vector<int> fs);
    ~DFA();
    State * find_state(int n);
    void add_state(int n);
    void add_transition(int s, char inp, int d);
    void move(char c);
    bool is_accepted();
    void reset();
    State *get_current_state();
    string to_string();
};
#endif
