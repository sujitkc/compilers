#ifndef SCANNER_H
#define SCANNER_H
#include <iostream>
#include <string>
#include <vector>
#include <map>

#include "dfa.h"

using namespace std;
class Scanner {
  public:
    static const unsigned int ASSIGN  = 1;
    static const unsigned int EQ      = 2;
    static const unsigned int ID      = 3;
    static const unsigned int KEYWORD = 4;
    static const unsigned int WS      = 5;

  protected:
    unsigned int position;
    string lexeme;
    string input;
    DFA& dfa;
    map<State *, unsigned int> tokens; // mapping the accepting states to the tokens to be returned.
 
  public:
    Scanner(DFA& _dfa, map<int, unsigned int> _tokens, string inp);
    virtual unsigned int next_token();
    vector<unsigned int> scan();
};
#endif
