#include <iostream>
#include <string>
#include <vector>
#include <map>

#include "scanner.h"

using namespace std;
Scanner::Scanner(DFA& _dfa, map<int, unsigned int> _tokens, string inp) : position(0), dfa(_dfa), lexeme(""), input(inp) {
  for(map<int, unsigned int>::iterator it = _tokens.begin(); it != _tokens.end(); ++it) {
    State *st = dfa.find_state(it->first);
    if(st == NULL) {
      throw ("Scanner construction failed : Illegal state id " + to_string(it->first) + " in tokens");
    }
    tokens[st] = it->second;
  }
}

/*
  Pseudocode:
    while(true)
      if the DFA can't move with the next symbol then raise 'lexical error'
      else
        move the DFA.
        next_lexeme += next_char
        if lookahead doesn't exist or exists but can't cause a move then
          lexeme = next_lexeme;
          if tokens[DFA.state] exists then return that
          else raise 'lexical error'
        else
          continue
    end while

*/
unsigned int Scanner::next_token() {

  string next_lexeme = "";
  while(position < input.size()) {
    dfa.move(input[position]);
    next_lexeme += input[position];
    position++;
    bool token_end = false;
    if(position == input.size()) { // end of string reached
      token_end = true;
    }
    else if(dfa.get_current_state()->find_next_state(input[position]) == NULL) {
      // would the lookahead char lead to successful transition? - No!
      token_end = true;
    }
    if(token_end == true) {
      // Can't make a move. Am not in an accepting state. So, failing.
      if(tokens.find(dfa.get_current_state()) == tokens.end()) {
        throw "Lexical error";
      }
      else {
        int result = tokens[dfa.get_current_state()];
        dfa.reset();
        lexeme = next_lexeme;
        return result;
      }
    }
  }
  throw "end of input";
}

vector<unsigned int> Scanner::scan() {
  dfa.reset();
  vector<unsigned int> tokens;
  try{
    while(true) {
      unsigned int token = next_token();
      cout << "token = " << token << endl;
      tokens.push_back(token);
    }
  }
  catch(const char* s) {
    if(string(s) == "end of input") {
      cout << "end of input" << endl;
      return tokens;
    }
    else {
      throw;
    }
  }
}
