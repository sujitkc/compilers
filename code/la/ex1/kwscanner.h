#ifndef KWSCANNER_H
#define KWSCANNER_H
#include <iostream>
#include <string>
#include <vector>
#include <map>

#include "scanner.h"
#include "dfa.h"

using namespace std;
class KWScanner : public Scanner {
  protected:
    const vector<string> keywords;
  public:
    static const unsigned int KEYWORD      = 6;
  public:
    KWScanner(DFA& _dfa, map<int, unsigned int> _tokens, string inp);
    virtual unsigned int next_token();
};
#endif
