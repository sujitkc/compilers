#include <string>
#include <vector>
#include <map>

#include "kwscanner.h"

using namespace std;
KWScanner::KWScanner(DFA& _dfa, map<int, unsigned int> _tokens, string inp) : Scanner(_dfa, _tokens, inp), keywords (vector<string>{"int"}) {
}

unsigned int KWScanner::next_token() {
  unsigned int token = Scanner::next_token();
  cout << "lexeme = " << lexeme << endl;
  if(token != Scanner::ID) {
    return token;
  }
  for(unsigned int i = 0; i < keywords.size(); i++) {
    if(keywords[i] == lexeme) {
      return KEYWORD;
    }
  }
  return ID;
}
