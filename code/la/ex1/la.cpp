#include <iostream>
#include <string>
#include <vector>
#include <map>

#include "dfa.h"
#include "scanner.h"
#include "kwscanner.h"

using namespace std;

void t3() {
  // construction of the DFA - begin
  vector<int> states{0, 1, 2, 3, 4};
  vector<char> alphabet;
  alphabet.push_back('='); // 0
  for(unsigned int i = 'A'; i <= 'Z'; i++) { // 1-26
    alphabet.push_back(char(i));
  }
  for(unsigned int i = 'a'; i <= 'z'; i++) { // 27-52
    alphabet.push_back(char(i));
  }
  for(unsigned int i = '0'; i <= '9'; i++) { // 53-62
    alphabet.push_back(char(i));
  }
  alphabet.push_back(' ');  // 63
  alphabet.push_back('\t'); // 64

  vector<int> fs{1, 2, 3, 4};
  DFA dfa(alphabet, states, 0, fs);

  // adding transitions to the DFA - begin
  dfa.add_transition(0, '=', 1);
  dfa.add_transition(1, '=', 2);
  for(unsigned int i = 'A'; i <= 'Z'; i++) { // 1-26
    dfa.add_transition(0, char(i), 3);
    dfa.add_transition(3, char(i), 3);
  }
  for(unsigned int i = 'a'; i <= 'z'; i++) { // 27-52
    dfa.add_transition(0, char(i), 3);
    dfa.add_transition(3, char(i), 3);
  }
  for(unsigned int i = '0'; i <= '9'; i++) { // 53-62
    dfa.add_transition(3, char(i), 3);
  }
  dfa.add_transition(0, ' ', 4);  // 63
  dfa.add_transition(0, '\t', 4);  // 64
  dfa.add_transition(4, ' ', 4);  // 63
  dfa.add_transition(4, '\t', 4);  // 64
  // adding transitions to the DFA - end
  // construction of the DFA - end

  map<int, unsigned int> state_to_token;
  state_to_token[1] = Scanner::ASSIGN;
  state_to_token[2] = Scanner::EQ;
  state_to_token[3] = Scanner::ID;
  state_to_token[4] = Scanner::WS;

  Scanner scanner(dfa, state_to_token, "Hello = world == IIITB");
  vector<unsigned int> tokens = scanner.scan();
  for(unsigned int i = 0; i < tokens.size(); i++) {
    cout << tokens[i] << endl;
  }
}

void t4() {
  // construction of the DFA - begin
  vector<int> states{0, 1, 2, 3, 4};
  vector<char> alphabet;
  alphabet.push_back('='); // 0
  for(unsigned int i = 'A'; i <= 'Z'; i++) { // 1-26
    alphabet.push_back(char(i));
  }
  for(unsigned int i = 'a'; i <= 'z'; i++) { // 27-52
    alphabet.push_back(char(i));
  }
  for(unsigned int i = '0'; i <= '9'; i++) { // 53-62
    alphabet.push_back(char(i));
  }
  alphabet.push_back(' ');  // 63
  alphabet.push_back('\t'); // 64

  vector<int> fs{1, 2, 3, 4};
  DFA dfa(alphabet, states, 0, fs);

  // adding transitions to the DFA - begin
  dfa.add_transition(0, '=', 1);
  dfa.add_transition(1, '=', 2);
  for(unsigned int i = 'A'; i <= 'Z'; i++) { // 1-26
    dfa.add_transition(0, char(i), 3);
    dfa.add_transition(3, char(i), 3);
  }
  for(unsigned int i = 'a'; i <= 'z'; i++) { // 27-52
    dfa.add_transition(0, char(i), 3);
    dfa.add_transition(3, char(i), 3);
  }
  for(unsigned int i = '0'; i <= '9'; i++) { // 53-62
    dfa.add_transition(3, char(i), 3);
  }
  dfa.add_transition(0, ' ', 4);  // 63
  dfa.add_transition(0, '\t', 4);  // 64
  dfa.add_transition(4, ' ', 4);  // 63
  dfa.add_transition(4, '\t', 4);  // 64
  // adding transitions to the DFA - end
  // construction of the DFA - end

  map<int, unsigned int> state_to_token;
  state_to_token[1] = Scanner::ASSIGN;
  state_to_token[2] = Scanner::EQ;
  state_to_token[3] = Scanner::ID;
  state_to_token[4] = Scanner::WS;

  KWScanner scanner(dfa, state_to_token, "Hello = world == int");
  vector<unsigned int> tokens = scanner.scan();
  for(unsigned int i = 0; i < tokens.size(); i++) {
    cout << tokens[i] << endl;
  }
}
int main() {
  t3();
  t4();
  return 0;
}
