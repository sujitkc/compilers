#include <iostream>

using namespace std;

int *f() {
  int i = 10;
  return &i;
}

int main() {
  cout << *(f()) << endl;
  return 0;
}
