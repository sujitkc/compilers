exception TypeError of string

(* Language Abstract Syntax -- Begin *)
type expr =
  | Id        of string
  | IntConst  of int
  | Add       of expr * expr
  | If        of expr * expr * expr
  | Let       of string * expr * expr
  | BoolConst of bool
  | Not       of expr
  | And       of expr * expr
  | Equals    of expr * expr
  | FunApp    of string * expr

type definition =
  | FunDef of string * string * expr (* let f x = e *)
  | VarDef of string * expr          (* let x = e   *)

(* Language Abstract Syntax -- End *)

type type_expr =
    BasicType    of string
  | UnaryOp      of string * type_expr
  | BinOp        of string * type_expr * type_expr
  | TypeVariable of string

let rec string_of_type_expr = function
    BasicType(s)     -> s
  | UnaryOp(s, t) -> s ^ " ( " ^ (string_of_type_expr t) ^ " ) "
  | BinOp(s, t1, t2) -> s ^ " ( " ^ (string_of_type_expr t1) ^ " , " ^ (string_of_type_expr t2) ^ " ) "
  | TypeVariable(s)  -> s

let rec string_of_expr = function
  | Id(vname)             -> vname
  | IntConst(n)           -> string_of_int n
  | Add(e1, e2)           -> (string_of_expr e1) ^ " + " ^ (string_of_expr e2)
  | If(b, e1, e2)         -> "if (" ^ (string_of_expr b) ^ ") then (" ^ (string_of_expr e1) ^ ") else (" ^ (string_of_expr e2) ^ ")"
  | Let(vname, e1, e2)    -> "let " ^ vname ^ " = (" ^ (string_of_expr e1) ^ ") in (" ^ (string_of_expr e2) ^ ")"
  | BoolConst(b)          -> string_of_bool b
  | Not(e)                -> "not(" ^ string_of_expr e ^ ")"
  | And(e1, e2)           -> "(" ^ (string_of_expr e1) ^ ") and (" ^ (string_of_expr e2) ^ ")"
  | Equals(e1, e2)        -> "(" ^ (string_of_expr e1) ^ ") = (" ^ (string_of_expr e2) ^ ")"
  | FunApp(fname, arg)    -> "(" ^ fname ^ ") (" ^ (string_of_expr arg) ^ ")"

module Sub = struct
  type type_set = {
    types : type_expr list;
    representative : type_expr;
  }

  let string_of_type_set ts =
    let set_strings = List.map 
      (
        fun te -> "    " ^ 
        (
          (string_of_type_expr te) ^ 
            if te = ts.representative then " *\n"
            else "\n"
        )
      )
      ts.types in
    let set_string = List.fold_left (fun a b -> a ^ b) "" set_strings in
    "  [\n" ^ set_string ^ "  ]\n"

  let string_of_sub sub =
    let set_strings = List.map string_of_type_set sub in
    let set_string = List.fold_left (fun a b -> a ^ b) "" set_strings in
    "[\n" ^ set_string ^ "]\n"
    
  let add_entry texp sub =
      let s = {
        types = [texp];
        representative = texp;
      } in
      s :: sub

  (* New substitution graph pre-populated with boolean and int types *)
  let new_sub () =
    let s1 = add_entry (BasicType("boolean")) [] in
    add_entry (BasicType("int")) s1

  let min_type t1 t2 =
    match t1, t2 with
      BasicType(s1), BasicType(s2) when s1 = s2 -> t1
    | UnaryOp(_, _), UnaryOp(_, _)
    | BinOp(_, _, _), BinOp(_, _, _)
    | TypeVariable(_), TypeVariable(_)
    | _, TypeVariable(_) -> t1
    | TypeVariable(_), _ -> t2
    | _, _ -> failwith ("min_type failed trying to compare two incompatible types " ^
                (string_of_type_expr t1) ^ 
                " and " ^ (string_of_type_expr t2) ^ ".")

  (* Given two sets, merges them to create one, and re-elects a common minimum type. *)
  let union s1 s2 sub =
    let s = {
       types = List.append s1.types s2.types;
       representative = min_type s1.representative s2.representative;
     } in

    let rec remove s = function
      [] -> []
    | h :: t -> if h = s then t else h :: (remove s t)

    in
    s :: (remove s2 (remove s1 sub))
 
  (* Given a type expression, return the set in which it is contained. *) 
  let rec find t = function
      [] -> failwith ("type " ^ (string_of_type_expr t) ^ " not found in substitution.")
    | s :: other_sets ->
      if List.mem t s.types then s
      else find t other_sets
 
  let rec unify m n sub =
    print_endline ("unifying " ^ (string_of_type_expr m) ^ " and " ^
      (string_of_type_expr n));
    let mset = (find m sub)
    and nset = (find n sub) in 
    if mset = nset then true, sub
    else
      match mset.representative, nset.representative with
        BasicType(t1), BasicType(t2) when t1 = t2                 ->
          true, sub
      | UnaryOp(op1, s), UnaryOp(op2, t) when op1 = op2           ->
          let s' = (union mset nset sub) in
          (unify s t s')
      | BinOp(op1, s1, s2), BinOp(op2, t1, t2) when op1 = op2     ->
          let s' = (union mset nset sub) in
          let tf1, s'' = (unify s1 t1 s') in
            if tf1 = false then
              false, s'
            else
              (unify s2 t2 s'')
      | TypeVariable(x), TypeVariable(y)                          ->
          true, (union mset nset sub)
      | TypeVariable(_), _                                        ->
          true, (union nset mset sub)
      | _, TypeVariable(_)                                        ->
          true, (union mset nset sub)
      | _, _                                                      ->
          false, sub

  (*
    return the equivalent type expression based on the minimum/representative
     types. For example:
     for sub = [ { t1, int*}; {t2, bool*}], BinOp(t1, t2) --> BinOp(int, bool)
  *)
  let rec normalise te sub =
    match te with
      BasicType(_) -> te
    | UnaryOp(op, t) -> UnaryOp(op, (normalise t sub))
    | BinOp(op, t1, t2) -> BinOp(op, (normalise t1 sub), (normalise t2 sub))
    | TypeVariable(_) -> (find te sub).representative
end

(* Symbol table/Environment -- Begin *)
module TypeEnv = struct
  let empty_env () = []

  let add_binding x v env =
    (x, v) :: env

  let rec lookup x env =
    match env with
      [] -> raise Not_found
    | (vname, value) :: env' ->
      if x = vname then value
      else (lookup x env')

  let rec string_of_env env =
    let rec iter = function
        [] -> ""
      | (vname, value) :: env' -> "(" ^ vname ^ "-->" ^ (string_of_type_expr value) ^ "); " ^ (string_of_env env')
    in
    "[" ^ (iter env) ^ "]"
end
(* Symbol table/Environment -- End *)


let new_typevar = 
  let var_count = ref 0 in
  fun () ->
    var_count := !var_count + 1;
    TypeVariable("t" ^ (string_of_int !var_count))  

let string_of_alltypes alltypes =
  let strings = List.map (
    fun (e,t) -> "  " ^ (string_of_expr e) ^ "===>" ^ (string_of_type_expr t) ^ "\n"
  ) alltypes in
  List.fold_left (fun x y -> x ^ y) "" strings

(* Functions for type inferencing - begin *)
(* Type inferencing expression *)
let rec infer_expr e tenv all_types sub =
  match e with
  | Id(vname) ->
      let nt        = new_typevar()                      in
      let sub'      = Sub.add_entry nt sub               in
      let at'       = TypeEnv.add_binding e nt all_types in
      let decl_ty   = TypeEnv.lookup vname tenv          in
      let tf, sub'' = Sub.unify nt decl_ty sub'          in
      if tf = true then at', sub''
      else failwith "error"
  | IntConst(_) ->
      let at' = (e, BasicType("int")) :: all_types in
      at', sub
  | BoolConst(_) ->
      let at' = (e, BasicType("boolean")) :: all_types in
      at', sub
  | Not(e) ->
      (* issue: should infer e's type here. *)
      let at' = (e, BasicType("boolean")) :: all_types in
      at', sub
  | Add(e1, e2) ->
      let at1, sub1 = infer_expr e1 tenv all_types sub in
      let at2, sub2 = infer_expr e2 tenv at1 sub1 in
      let t1 = (Sub.find (TypeEnv.lookup e1 at2) sub2).Sub.representative
      and t2 = (Sub.find (TypeEnv.lookup e2 at2) sub2).Sub.representative in
      begin
        match t1, t2 with
          BasicType(s1), BasicType(s2) when s1 = s2 ->
            let at' = TypeEnv.add_binding e (BasicType(s1)) at2 in
            at', sub2
        | _ ->
          print_endline ("t1 = " ^ (string_of_type_expr t1));
          print_endline ("t2 = " ^ (string_of_type_expr t2));
          print_endline (string_of_alltypes at2);
          print_endline (Sub.string_of_sub sub2);
          failwith ("type inferencing failed for " ^ (string_of_expr e))
      end
  | And(e1, e2) as e ->
      let at1, sub1 = infer_expr e1 tenv all_types sub in
      let at2, sub2 = infer_expr e2 tenv at1 sub1 in
      let t1 = TypeEnv.lookup e1 at2
      and t2 = TypeEnv.lookup e2 at2 in
      begin
        match t1, t2 with
          BasicType("boolean"), BasicType("boolean") ->
            let at' = TypeEnv.add_binding e (BasicType("boolean")) at2 in
            at', sub2
        | _ -> failwith ("type inferencing failed for " ^ (string_of_expr e))
      end
  | If(b, e1, e2) ->
      let atb, subb = infer_expr b tenv all_types sub           in
      let btype = TypeEnv.lookup b atb                          in
      let _, sub3 = Sub.unify btype (BasicType("boolean")) subb in
      let norm_btype = (Sub.normalise btype sub3)               in
      if norm_btype = BasicType("boolean") then
        let at1, sub1 = infer_expr e1 tenv atb sub3 in
        let at2, sub2 = infer_expr e2 tenv at1 sub1 in
        let t1 = TypeEnv.lookup e1 at2
        and t2 = TypeEnv.lookup e2 at2              in
        let tf, sub' = Sub.unify t1 t2 sub2         in
        if tf = true then
        (TypeEnv.add_binding e t1 at2), sub'
        else 
          failwith (
            "type inferencing failed to unify the two branches in " ^
            (string_of_expr e))
      else
        failwith ("Non boolean condition in " ^
          (string_of_expr e) ^ ": " ^ (string_of_type_expr norm_btype))
  | Let(vname, e1, e2) ->
      let nt        = new_typevar()                     in
      let sub'      = Sub.add_entry nt sub
      and tenv'     = TypeEnv.add_binding vname nt tenv in
      let at1, sub1 = infer_expr e1 tenv all_types sub' in
      let t1        = TypeEnv.lookup e1 at1             in
      let tf, sub'' = Sub.unify nt t1 sub1              in
      if tf = true then
        let at, sub2 = infer_expr e2 tenv' at1 sub'' in
        let t2 = TypeEnv.lookup e2 at                in
        (TypeEnv.add_binding e t2 at), sub2
      else
        failwith ("type inferencing failed to unify the two branches in " ^
          (string_of_expr e))
  | Equals(e1, e2) ->
      let at1, sub1 = infer_expr e1 tenv all_types sub    in
      let at2, sub2 = infer_expr e2 tenv at1 sub1         in
      let t1 = Sub.normalise (TypeEnv.lookup e1 at2) sub2
      and t2 = Sub.normalise (TypeEnv.lookup e2 at2) sub2 in
      let tf, sub' = Sub.unify t1 t2 sub2                 in
      if tf = true then
        (TypeEnv.add_binding e (BasicType("boolean")) at2), sub2
      else
        failwith ("type inferencing failed to unify the two sides in " ^
          (string_of_expr e))
  | FunApp(fname, arg) ->
      let ftype = TypeEnv.lookup fname tenv    in
      let norm_ftype = Sub.normalise ftype sub in
      begin
        match norm_ftype with
          BinOp("->", t1, t2) ->
            let at1, sub1 = infer_expr arg tenv all_types sub in
            let arg_type = TypeEnv.lookup arg at1             in
            let norm_arg_type = (Sub.normalise arg_type sub1) in
            let _, sub2 = Sub.unify t1 norm_arg_type sub1     in
            (TypeEnv.add_binding e t2 at1), sub2
        | _ ->
            failwith (fname ^ " is not a function type : " ^ 
              (string_of_type_expr norm_ftype))
      end

  | _ -> raise (TypeError "Sorry! Not implemented!")

(* Type inferencing definitions *)
let infer_def def tenv sub =
  match def with
  | VarDef(vname, e) ->
    let at, sub' = infer_expr e tenv [] sub in
    let t = TypeEnv.lookup e at             in
    let norm_type = Sub.normalise t sub'    in
    (TypeEnv.add_binding vname norm_type tenv)
    
  | FunDef(fname, arg, body) ->
    let arg_typevar = new_typevar ()                     in
    let sub1 = Sub.add_entry arg_typevar sub             in
    let tenv1 = TypeEnv.add_binding arg arg_typevar tenv in
    let at2, sub2 = infer_expr body tenv1 [] sub1        in
    let body_type = TypeEnv.lookup body at2              in
    let ftype = BinOp("->", arg_typevar, body_type)      in
    let norm_ftype = Sub.normalise ftype sub2            in
    TypeEnv.add_binding fname norm_ftype tenv

(* Type inferening program *)
let infer_program prog =
  
  let rec infer p tenv =
    match p with
      [] -> tenv
    | def :: defs ->
      let tenv' = infer_def def tenv (Sub.new_sub ()) in
      infer defs tenv'
  in
  infer prog []

(* Functions for type inferencing - end *)

(********** TEST CASES *********************************)
(* to test initialisation of substitution graph *)
let t1 () =
  let sub = Sub.new_sub () in
  print_endline "t1 ...";
  print_endline (Sub.string_of_sub sub)

(* to test min_type with boolean *)
let t2 () =
  let t1 = BasicType("boolean")
  and t2 = BasicType("boolean")
  in
  let min = Sub.min_type t1 t2 in
  print_endline "t2 ...";
  print_endline (string_of_type_expr min)

(* to test min_type with int *)
let t3 () =
  let t1 = BasicType("int")
  and t2 = BasicType("int")
  in
  let min = Sub.min_type t1 t2 in
  print_endline "t3 ...";
  print_endline (string_of_type_expr min)

(* to test min_type with int and boolean *)
let t4 () =
  print_endline "t4 ...";
  let t1 = BasicType("boolean")
  and t2 = BasicType("int")
  in
  try
    let min = Sub.min_type t1 t2 in
    print_endline (string_of_type_expr min)
  with
    Failure(s) ->
      print_endline s

(* to test min_type with boolean and type variable *)
let t5 () =
  print_endline "t5 ...";
  let t1 = BasicType("boolean")
  and t2 = TypeVariable("t1")
  in
  try
    let min = Sub.min_type t1 t2 in
    print_endline (string_of_type_expr min)
  with
    Failure(s) ->
      print_endline s

(* to test min_type with boolean and type variable *)
let t6 () =
  print_endline "t6 ...";
  let t1 = BasicType("boolean")
  and t2 = TypeVariable("t1")
  in
  try
    let min = Sub.min_type t2 t1 in
    print_endline (string_of_type_expr min)
  with
    Failure(s) ->
      print_endline s

(* to test min_type with boolean and type variable *)
let t7 () =
  print_endline "t7 ...";
  let t1 = BinOp("->", BasicType("boolean"), BasicType("int"))
  and t2 = TypeVariable("t1")
  in
  try
    let min = Sub.min_type t2 t1 in
    print_endline (string_of_type_expr min)
  with
    Failure(s) ->
      print_endline s

(* to test union - type variable and boolean *)
let t8 () =
  print_endline "t8 ...";
  let sub = Sub.new_sub () in
  let sub' = Sub.add_entry (TypeVariable("t1")) sub in
  let s1 = Sub.find (TypeVariable("t1")) sub'
  and s2 = Sub.find (BasicType("boolean")) sub' in
  let sub'' = Sub.union s1 s2 sub' in
  print_endline (Sub.string_of_sub sub'')

(* to test union - type variable and type variable *)
let t9 () =
  print_endline "t9 ...";
  let sub = Sub.new_sub () in
  let sub'  = Sub.add_entry (TypeVariable("t1")) sub in
  let sub'' = Sub.add_entry (TypeVariable("t2")) sub' in
  let s1 = Sub.find (TypeVariable("t1")) sub''
  and s2 = Sub.find (TypeVariable("t2")) sub'' in
  let sub''' = Sub.union s1 s2 sub'' in
  print_endline (Sub.string_of_sub sub''')

(* to test union - type variable and type variable and then with boolean *)
let t10 () =
  print_endline "t10 ...";
  let sub = Sub.new_sub () in
  let sub'  = Sub.add_entry (TypeVariable("t1")) sub in
  let sub'' = Sub.add_entry (TypeVariable("t2")) sub' in
  let s1 = Sub.find (TypeVariable("t1")) sub''
  and s2 = Sub.find (TypeVariable("t2")) sub'' in
  let sub''' = Sub.union s1 s2 sub'' in
  let s3 = Sub.find (TypeVariable("t2")) sub'''
  and s4 = Sub.find (BasicType("boolean")) sub''' in
  let sub'''' = Sub.union s3 s4 sub''' in
  print_endline (Sub.string_of_sub sub'''')

(* to test unification of two BinOp type expressions. *)
let t11 () =
  print_endline "t11 ...";
  let sub1 = Sub.new_sub () in
  let sub2  = Sub.add_entry (TypeVariable("t1")) sub1 in
  let sub3 = Sub.add_entry (TypeVariable("t2")) sub2 in
  let t1 = BinOp("->", TypeVariable("t1"), TypeVariable("t2"))
  and t2 = BinOp("->", BasicType("int"), BasicType("boolean")) in
  let sub4 = Sub.add_entry t1 sub3 in
  let sub5 = Sub.add_entry t2 sub4 in
  print_endline (Sub.string_of_sub sub5);
  let tf, sub6 = Sub.unify t1 t2 sub5 in
  print_endline (Sub.string_of_sub sub6)

let test_infer_expr e =
  let at, sub = infer_expr e [] [] (Sub.new_sub ())in
  print_endline (Sub.string_of_sub sub);
  print_endline (string_of_alltypes at)

(*
  to test the type inference of an if expression:
    if true then 1 else 2
*)
let t12 () =
  print_endline "t12 ...";
  let e =
    If(
      BoolConst(true),
      IntConst(1),
      IntConst(2)
    )
  in test_infer_expr e
 
(*
  to test the type inference of an let expression:
    let x = 1 in x + 1
*)
let t13 () =
  print_endline "t13 ...";
  let e =
    Let(
      "x",
      IntConst(1),
      Add(
        Id("x"),
        IntConst(1)
      )
    )
  in test_infer_expr e

(*
  to test the type inference of an if expression:
    if 1 = 2 then 1 else 2
*)
let t14 () =
  print_endline "t14 ...";
  let e =
    If(
      Equals(
        IntConst(1),
        IntConst(2)
      ),
      IntConst(1),
      IntConst(2)
    )
  in test_infer_expr e

let test_infer_def def =
  let at = infer_def def [] (Sub.new_sub ()) in
  print_endline (TypeEnv.string_of_env at) 

(* 
to test VarDef definition. Example
 let x = 1
*)
let t15 () =
  print_endline "t15 ...";
  let d1 = VarDef("x", IntConst(1)) in
  test_infer_def d1

(* 
to test VarDef definition. Example
 let x = 2 + 1
*)
let t16 () =
  print_endline "t16 ...";
  let d1 = VarDef("x", Add(IntConst(2), IntConst(1))) in
  test_infer_def d1

(* 
to test VarDef definition. Example
 let x = if 1 = 2 then 1 else 2
*)
let t17 () =
  print_endline "t17 ...";
  let d1 = VarDef("x",
    If(
      Equals(
        IntConst(1),
        IntConst(2)
      ),
      IntConst(1),
      IntConst(2)
    )
  ) in
  test_infer_def d1

(* 
to test FunDef definition. Example
 let f x = if x then 1 else 2
*)
let t18 () =
  print_endline "t18 ...";
  let d1 = FunDef("f", "x",
    If(Id("x"),
      IntConst(1),
      IntConst(2)
    )
  ) in
  test_infer_def d1

(* 
to test FunDef definition. Example
 let f x = if true then x else x
*)
let t19 () =
  print_endline "t19 ...";
  let d1 = FunDef("f", "x",
    If(BoolConst(true),
      Id("x"),
      Id("x")
    )
  ) in
  test_infer_def d1

let test_infer_program prog =
  let at = infer_program prog in
  print_endline (TypeEnv.string_of_env at)

(* 
to test program. Example
 let x = if 1 = 2 then 1 else 2
 let f y = if true then x else y
*)
let t20 () =
  print_endline "t20 ...";
  let d1 = VarDef("x",
    If(
      Equals(
        IntConst(1),
        IntConst(2)
      ),
      IntConst(1),
      IntConst(2)
    )
  )

  and d2 = FunDef("f", "y",
    If(BoolConst(true),
      Id("x"),
      Id("y")
    )
  ) in
  test_infer_program [d1; d2]

(* 
to test program. Example
 let x = if 1 = 2 then 1 else 2
 let f y = if y then x else 2
 let z = f false
*)
let t21 () =
  print_endline "t21 ...";
  let d1 = VarDef("x",
    If(
      Equals(
        IntConst(1),
        IntConst(2)
      ),
      IntConst(1),
      IntConst(2)
    )
  )

  and d2 = FunDef("f", "y",
    If(
      Id("y"),
      Id("x"),
      IntConst(2)
    )
  )
  and d3 = VarDef(
    "z",
    FunApp(
      "f",
      BoolConst(false)
    )
  )
  in
  test_infer_program [d1; d2; d3]

let _ = 
  t1 ();
  t2 ();
  t3 ();
  t4 ();
  t5 ();
  t6 ();
  t7 ();
  t8 ();
  t9 ();
  t10 ();
  t11 ();
  t12 ();
  t13 ();
  t14 ();
  t15 ();
  t16 ();
  t17 ();
  t18 ();
  t19 ();
  t20 ();
  t21 ();
  print_string "Hello world!\n"
