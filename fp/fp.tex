\documentclass{article}
\usepackage{xcolor}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{listings}
\usepackage{hyperref}
\usepackage{alltt}
\usepackage{tikz}
\usepackage{framed}
\usepackage{marvosym}
\usepackage{wasysym}
\usepackage{marvosym}
\usepackage[margin=0.75in]{geometry}

\usetikzlibrary{positioning,shapes,arrows, backgrounds, fit, shadows}
\usetikzlibrary{decorations.markings}
%\usepackage{wasysym}
%\usepackage{marvosym}
%\setbeamertemplate{footline}[frame number]
\begin{document}

\title{An Introduction to Functional Programming}
\author{Sujit Kumar Chakrabarti}
\date{}

\maketitle

\definecolor{lightblue}{rgb}{0.8,0.93,1.0} % color values Red, Green, Blue
\definecolor{darkblue}{rgb}{0.4,0.3,1.0} % color values Red, Green, Blue
\definecolor{Blue}{rgb}{0,0,1.0} % color values Red, Green, Blue
\definecolor{darkgreen}{rgb}{0,0.7,0.2} % color values Red, Green, Blue
\definecolor{Red}{rgb}{1,0,0} % color values Red, Green, Blue
\definecolor{Pink}{rgb}{0.7,0,0.2}
\definecolor{links}{HTML}{2A1B81}
\definecolor{mydarkgreen}{HTML}{126215}
\newcommand{\highlight}[1]{{\color{Red}(#1)}}
\newcommand{\comment}[1]{\begin{tiny}
{\color{blue}(#1)}
\end{tiny}}
\newcommand{\keyword}[1]{{\color{mydarkgreen}\textbf{\texttt{#1}}}}
\newcommand{\code}[1]{
	{\color{mydarkgreen}
		\begin{alltt}
			{#1}
		\end{alltt}
	}
}
\newcommand{\myheader}[1]{
	{\color{darkblue}
		\begin{Large}
			\begin{center}
				{#1}
			\end{center}
		\end{Large}
	}
}
\newcommand{\myminorheader}[1]{
	{\color{purple}
		\begin{large}
			{#1}
		\end{large}
	}
}


\newcommand{\mynote}[1]{
\vspace{1cm}
\textbf{Note:} \\
	\fbox{
		\parbox{\textwidth}{%
			\colorbox{green!25}{
				\begin{minipage}{\textwidth}
					{#1}
				\end{minipage}
			}
		}
	}
}

\subsection{Launching OCaml}
\begin{lstlisting}[frame=single]
ledit -x -h ~/.ocaml_history ocaml
        Objective Caml version 3.12.1
\end{lstlisting}


\section*{Variables, Types and \lstinline@let@ Bindings}
\begin{lstlisting}[frame=single]
let x = 1;;
\end{lstlisting}
A \lstinline@let@ expression creates a binding of a value to a name. The above expression creates a variable named \lstinline@x@ in the top-level environment whose value is 1. The interesting part is that the type of \lstinline@x@ is determined by type inferencing to be integer, since the value assigned to it is of the type integer.

\begin{lstlisting}[frame=single]
let tf = true || false;;
let y = 1.0;;
\end{lstlisting}
Likewise, there are other native (atomic) types like \lstinline@bool@, \lstinline@float@, \lstinline@string@, \lstinline@char@, and so on. Above, we create a variable \lstinline@tf@ of the type \lstinline@bool@, a variable \lstinline@y@ of the type \lstinline@float@ respectively by assigning to them expressions which evaluate to values of these types.

\subsection{Operations}
\begin{lstlisting}[frame=single]

\end{lstlisting}

\section*{Lists}
Lists are of fundamental importance in all functional programming. A list is defined as follows:
\begin{lstlisting}[frame=single]
[1;2;3;4];;
\end{lstlisting}
In Ocaml, lists are homogeneous, which means that all elements of a list should be of the same type. Lists are appropriate to represent variable sized ordered collections of elements of the same type.

Some operations defined on lists (in the \lstinline@List@ module of OCaml standard library) are as follows:
\lstinline@length@ function returns the number of elements in a list.
\begin{lstlisting}[frame=single]
List.length [1;2;3];;
\end{lstlisting}

In functional programming, the head of a list is its first element, and the tail is a list containing the remaining elements. In Ocaml, the head and tail of a list are given by the the \lstinline@hd@ and \lstinline@tl@ functions of the \lstinline@List@ module.
\begin{lstlisting}[frame=single]
let l = [1;2;3];;
List.hd l;;
List.tl l;;
\end{lstlisting}

An empty list is denoted by \lstinline@[]@. We can check if a list is empty by the following:
\begin{lstlisting}[frame=single]
l = [];;
\end{lstlisting}

The list construction (\lstinline@::@) operator is used to create bigger lists from smaller ones:
\begin{lstlisting}[frame=single]
let l1 = ["a"; "b"; "c"];;
"z"::l1;;
\end{lstlisting}

As can be expected, the following will lead to a type error, as we are trying to construct a list out of incompatible types.
\begin{lstlisting}[frame=single]
1::l1;;
\end{lstlisting}

It is to be noted that the construction operator doesn't lead to any modification in the value of the existing list. It just creates a new one. In fact, immutability of data is one of the central features of functional programming. More about this below.
\begin{lstlisting}[frame=single]
l1;;
\end{lstlisting}

\section*{Tuples}
Tuples are similar to lists, and are defined as follows:
\begin{lstlisting}[frame=single]
(1, 2, "IIITB");;
\end{lstlisting}
As seen above, unlike lists, tuples are permitted to be heterogeneous, as in the elements of a tuple can be of arbitrary types. Tuples are appropriate to represent composite data of possibly differently typed components, and fixed size.

\section*{Functions}
\begin{lstlisting}[frame=single]
let add x y = x + y;;
\end{lstlisting}

Functions are defined as above, again using \lstinline@let@ bindings. In the above, we create a function named \lstinline@add@ which takes two parameters \lstinline@x@ and \lstinline@y@. The return value is the same the expression computed in the function body.

A function is called as follows: the function name, followed by the arguments; no comma separators, no enclosing parentheses.
\begin{lstlisting}[frame=single]
add 1 2;;
\end{lstlisting}

The following expression causes a type error, because \lstinline@add@ above is defined for adding two integer parameters. This is infered by the type inferencer from the \lstinline@+@ operator used in the function body.
\begin{lstlisting}[frame=single]
add 1. 2.;;
\end{lstlisting}

To define a function which adds two floats, we need to define another function \lstinline@addf@ which adds two floats with the \lstinline@+.@ operator.

\begin{lstlisting}[frame=single]
let addf x y = x +. y;;
\end{lstlisting}

An anonymous function -- a lambda in many other programming languages, e.g. Lisp, Python etc. -- is defined as follows:
\begin{lstlisting}[frame=single]
fun x y -> x + y;;
\end{lstlisting}

Creating an anonymous function and binding it to a name is synonymous to defining a function as shown below:
\begin{lstlisting}[frame=single]
let f = fun x y -> x + y;;
f 100 200;;
\end{lstlisting}

\section{Recursive Functions}
\begin{lstlisting}[frame=single]

let rec fact n = if n = 1 then 1 else n * (fact (n - 1));;
fact 4;;
let rec len l = if l = [] then 0 else 1 + (len (List.tl l));;
len [1;2;3];;

let cons a lst = a :: lst;;
maketuple a b = (a, b);;
let maketuple a b = (a, b);;
maketuple 1 "Sujit";;
maketuple 'a' true;;
x;;
let givex () = x;;
givex();;
let x = 2;;
givex();;
x;;
type shape =
 Circle of int
| Square of int
| Rectangle of int * int;;
1 < 2;;
1 <= 2;;
type shape =
 Circle of float
| Square of float
| Rectangle of float * float;;
let c = Circle(10.);;
let s = Square(3);;
let s = Square(3.);;
let r = Rectangle(2., 3.);;
let area s =
match s with
  Circle(r) -> 3.14 * r * r
| Rectangle(l, b) -> l *. b
| Square(l) -> l *. l;;
let area s =
match s with
  Circle(r) -> 3.14 *. r *. r
| Rectangle(l, b) -> l *. b
| Square(l) -> l *. l;;
area c;;
area r;;
area s;;
let area s =
  Circle(r) -> 3.14 *. r *. r
| Rectangle(l, b) -> l *. b
| Square(l) -> let r = Rectangle(l, l) in (area r);;
let area s =
match s with
  Circle(r) -> 3.14 *. r *. r
| Rectangle(l, b) -> l *. b
| Square(l) -> let r = Rectangle(l, l) in (area r);;
area s;;
let len l =
match l with
 [] -> 0
| h :: t -> 1 + (len t);;
len l1;;
let rec m l =
match l with
 [] -> []
| h :: t -> (2 * h) :: (m t);;
m [1;2;3];;
let rec m f l =
match l with
 [] -> []
| h :: t -> (f h) :: (m f t);;
let mul2 x = 2 * x;;
m mul2 [1;2;3];;
let add2 x = x + 2;;
m add2 [1;2;3];;
List.map (fun x -> x + 2) [1;2;3];;
\end{lstlisting}
\end{document}
