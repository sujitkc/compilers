(* To illustrate closures using summation example *)

let sum zero term a next b plus =
  let rec innerSum a =
  if (a > b) then zero
  else            (plus (term a) (innerSum (next a)))
  in
  innerSum a

let sumnum a b =
  let zero     = 0
  and term x   = x
  and next x   = x + 1
  and plus x y = x + y
  in
  sum zero term a next b plus;;

let sumcube a b =
  let zero     = 0
  and term x   = x * x * x
  and next x   = x + 1
  and plus x y = x + y
  in
  sum zero term a next b plus;;

let sumpi a b =
  let zero     = 0.
  and term x   = (1. /. (x *. (x +. 2.)))
  and next x   = x +. 4.
  and plus x y = x +. y
  in
  sum zero term a next b plus

let sumchars a b =                              
    let zero     = ""                                 
    and term x   = (Char.escaped x)                 
    and next x   = char_of_int ((int_of_char x) + 1)
    and plus x y = x ^ y                          
    in                                            
    sum zero term a next b plus
