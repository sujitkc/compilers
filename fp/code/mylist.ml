let rec len l =
  match l with
    []     -> 0
  | _ :: t -> 1 + (len t)
