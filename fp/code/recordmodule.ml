module Student = struct
  type student = {
    name : string;
    rollNumber : int
  }
end

module Employee = struct
  type employee = {
    name : string;
    empId : string
  }
end
