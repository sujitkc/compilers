(* To illustrate higher order functions using map example *)

let rec map f lst =
  match lst with
    [] -> []
  | h :: t -> (f h) :: (map f t)
