class virtual person n =
object
  val name = n
  method virtual to_string : unit -> string
end

class employee n num dept =
object (self)
inherit person (n)
  val empNum = num
  val department   = dept
  method to_string () = "Employee: " ^ name ^ " " ^ string_of_int empNum ^ " " ^ department
end

class student n num =
object (self)
inherit person (n)
  val rollNum = num
  method to_string () = "Student: " ^ name ^ " " ^ string_of_int rollNum
end

class institute =
object
  val mutable people : (person list) = []
  method addPerson p = people <- p :: people

  method to_string () =
    let rec iter l =
      match l with
        [] -> ""
      | h :: t -> h#to_string () ^ "\n" ^ (iter t)
    in
    iter people
end

let t1 () =
  let i = new institute in
  let s = new student "Subin" 100 in
  let e = new employee "Sujit" 1252 "teach" in
  i#addPerson s;
  i#addPerson e;
  i#to_string ()
