(* To illustrate the record type *)
type student = {
  name      : string;
  age       : int;
  rollNumber: int
}

let s1 = {
  name       = "Sujit";
  age        = 38;
  rollNumber = 1252
}






























(* Field Access - Method 1 *)
let _ = s1.age;; (* - : int = 38 *)

(* Field Access - Method 2 *)
let {name = n; age = a; rollNumber = r} = s1;;
(*
  val n : string = "Sujit"
  val a : int = 38
  val r : int = 1252
*)

(* Creating new instances using 'with' *)
let s2 = {s1 with name = "Sujit"};;
(* val s2 : student = {name = "Sujit"; age = 38; rollNumber = 1252} *)

s2;; (*- : student = {name = "Sujit"; age = 38; rollNumber = 1252}  *)

