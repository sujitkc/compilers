(* To illustrate recursive containment. *)
class linkedlist v tl =
  object
  val value = 0
  val tail  = tl

  method find amount = balance := !balance + amount
  method debit amount  = balance := !balance - amount
  end
