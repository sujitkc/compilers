(* To illustrate higher order functions using summation example *)

let rec sumnum1 a b =
  if (a > b) then 0
  else            a + (sumnum1 (a + 1) b)

let rec sumcube1 a b =
  if (a > b) then 0
  else            a * a * a + (sumcube1 (a + 1) b)

let rec sumpi1 a b =
  if (a > b) then 0.
  else           (1. /. (a *. (a +. 2.))) +. (sumpi1 (a +. 4.) b)






































let rec sumchars1 a b =
    if a > b then ""                                                    
    else (Char.escaped a) ^ (sumchars1 (char_of_int ((int_of_char a) + 1)) b)
