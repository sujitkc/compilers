let rec isEven n = 
  if n = 0 then true 
  else (isOdd (n - 1))
;;

let rec isOdd n =           
  if n = 0 then false 
  else (isEven (n - 1))
;;
