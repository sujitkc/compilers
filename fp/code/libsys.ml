class title n =
object
  val name : string = n

  method getName () = name

  method toString () = name
end

exception Not_allowed of string

class library =
object (self)
  val mutable titles : (title * (string ref)) list = []
  
  val issued    = "issued"
  val notissued = "notissued"

  method addTitle (t) = titles <- ((t, ref "notissued") :: titles)

  method issue (entry : (title * (string ref))) =
   let (tit, status) = entry 
   and initialMessage = "issueTitle failed for title '" in
   if !status = issued then
      raise (Not_allowed ( initialMessage ^ (tit#getName ()) ^ "'"))
    else
      (status := issued)

  method issueTitle (t : title) =
    let rec iter tlist =
      match tlist with
        [] -> raise Not_found
      | (tit, status) :: _ when (tit#getName ()) = (t#getName ()) ->
        self#issue (tit, status)           
      | _ :: tl -> (iter tl)
    in 
    (iter titles)

  method printAllTitles () =
    let rec iter tlist =
      match tlist with
        [] -> ()
      | (tit, status) :: t -> (print_string (tit#toString () ^ " " ^ !status); print_string "\n"; (iter t))
    in
    (iter titles)
end

class book n a =
object (self)
inherit title (n) as super
  val author : string = a

  method getAuthor () = author
  method toString () = super#toString () ^ " " ^ self#getAuthor ()
end

let t1 () =
  let l = new library               
  and t1 = new title "War and Peace"
  and t2 = new title "Geetanjali"
  and t3 = new book "Rangabhoomi" "Premchand"
  in
  l#addTitle t1;
  l#addTitle t2;
  l#printAllTitles();
  l#issueTitle t1; 
  l#printAllTitles()
