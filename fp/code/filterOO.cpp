#include <iostream>
#include <vector>

using namespace std;

class BooleanFunction {
public: virtual bool execute(int n) = 0;
};

class isOdd : public BooleanFunction {
public: virtual bool execute(int n) { return ((n % 2) == 1); }
};

class isEven : public BooleanFunction {
public: virtual bool execute(int n) { return ((n % 2) == 0); }
};

vector<int> filterOO(BooleanFunction& f, vector<int> array) {
	vector<int> result;
	for(unsigned int i = 0; i < array.size(); i++) {
		if(f.execute(array[i])) {
			result.push_back(array[i]);
		}
	}
	return result;
}

void printIntVector(vector<int> v) {
	cout << "[";
	for(unsigned int i = 0; i < v.size(); i++) {
		cout << v[i] << ", ";
	}
	cout << "]" << endl;
}

int main() {
	int inp[] = {1, 2, 3, 4, 5};
	vector<int> v(inp, inp + 5); 

	isOdd fOdd;
	isEven fEven;
	vector<int> r1 = filterOO(fOdd, v);
	vector<int> r2 = filterOO(fEven, v);

	printIntVector(r1);
	printIntVector(r2);

	return 0;
}
