(* To illustrate the shadowing of fields due to top-level namespace visibility of fields *)

type student = {
  name      : string;
  age       : int;
  rollNumber: int
}
  
let s1 = {
  name       = "Sujit";
  age        = 38;
  rollNumber = 1252
}

type vehicle = {
  name         : string; (* shadows the student::name field *)
  registration : string;
}

