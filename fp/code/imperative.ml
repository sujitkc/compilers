(* To illustrate imperative programming using reference cells and loops *)

let factorial n =
  let prod = ref 1 in
  for i = 1 to n do
    prod := !prod * i
  done;
  !prod


let power x y =
  let result = ref 1 in
  for i = 1 to y do 
    result := !result * x
  done;
  !result
