(* To illustrate mutable fields. *)

exception Not_allowed of string

class account =
  object
  val mutable balance = 0

  method getBalance () = balance
  method credit amount = balance <- balance + amount
  method debit amount  = 
    if amount > balance then
      raise (Not_allowed "Debit amount exceeds balance.")
    else
      balance <- balance - amount
  end

class customer a =
  object
  val accnt : account = a

  method getAccount () = accnt

  end

class init_account =
  object (self)
  inherit account as super

  initializer
    print_string ("New account created with balance " ^ (string_of_int balance) ^ ".\n");
  end



(* basic functionality *)
let t1_bank () =
  let a = new account     in
  a#credit 200;
  let _ = a#getBalance () in
  let _ = a#debit 100     in
  a#getBalance ()

(* Not_allowed - trying to debit more than balance *)
let t2_bank () =
  let a = new account     in
  a#credit 200;
  let _ = a#getBalance () in
  let _ = a#debit 300     in
  a#getBalance ()

