(* An example of implementation of design-patterns through higher order functions and closures.
  This program implements template method design pattern. *)

(* template function *)
let tester compile execute =
  fun () ->
    if(compile ()) then 
      execute()
    else
      false

(* Java Tester *)
let javatester fileName =
  let compile () =
    (print_string ("compiling Java file: " ^ fileName ^ ".java\n")); true
  
  and execute () =
    (print_string ("executing Java file: " ^ fileName ^ ".class\n")); true
  in
  tester compile execute

(* C Tester *)
let ctester fileName =
  let compile () =
    (print_string ("compiling C file: " ^ fileName ^ ".c\n")); true
  
  and execute () =
    (print_string ("executing C file: " ^ fileName ^ ".exe\n")); true
  in
  tester compile execute

(* Client *)
let clientFunction testers =
  print_string "Client does some other stuff.\n";
  let rec loop t =
  match t with
    [] -> true
  | h :: t -> if h() = true then (loop t) else false
  in
  loop testers

(* Driver *)
let main () =
  let jt = javatester "f1"
  and ct = ctester "f2"
  in
  let _ = clientFunction [jt; ct]
  in ()
