(* To illustrate tail-recursion *)

(* Not tail recursive *)
let rec map1 f lst =
  match lst with
    []     -> []
  | h :: t -> (f h) :: map1 f t


































(* Tail recursive *)
let map2 f lst =
  let rec iter f lst acc =
    match lst with [] -> acc
  | h :: t             -> iter f t (acc @ [(f h)])
  in
  iter f lst []
