#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Tester {
protected:
	string fileName;
public:
	Tester(string fn) : fileName(fn) {}

	virtual bool compile() = 0;
	virtual bool execute() = 0;	

	// Template method
	bool test() {
		if(compile()) {
			return execute();
		}
		return false;
	}
};

class JavaTester : public Tester {
public:
	JavaTester(string fn) : Tester(fn) {}

	virtual bool compile() {
		cout << "Compiling Java file: " << fileName << ".java" << endl;
		return true;
	}

	virtual bool execute() {
		cout << "Executing class file: " << fileName << ".class" << endl;
	}
};

class CTester : public Tester {
public:
	CTester(string fn) : Tester(fn) {}

	virtual bool compile() {
		cout << "Compiling C file: " << fileName << ".c" << endl;
		return true;
	}

	virtual bool execute() {
		cout << "Executing class file: " << fileName << ".exe" << endl;
	}
};

void clientFunction(vector<Tester*> testers) {
	cout << "Client function does some other stuff." << endl;
	for(unsigned int i = 0; i < testers.size(); i++) {
		testers[i]->test();
	}
}

int main() {
	JavaTester t1("f1");
	CTester t2("f2");
	vector<Tester*> testers;
	testers.push_back(&t1);
	testers.push_back(&t2);
	clientFunction(testers);

	return 0;
}
