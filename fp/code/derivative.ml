let derivative f =
  let dx = 0.00001
  in
  fun x -> ((f (x +. dx)) -. (f x)) /. dx

let d (f, x) =
  let dx = 0.00001
  in
  ((f (x +. dx)) -. (f x)) /. dx

let dd f = derivative (derivative f)
