let rec getOdds lst =
	if lst = [] then []
	else (List.hd lst)::(getEvens (List.tl lst))
and getEvens lst =
	if lst = [] then []
	else if (List.tl lst) = [] then []
		else (getOdds (List.tl lst))
;;

