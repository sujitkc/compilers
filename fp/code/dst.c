// To illustrate depth-subtyping
// Contains pseudo-C code

struct Part {
	char *content;
};

struct Section {
	char *content;
};

struct Chapter {
	char *content;
	char *refs[];
};

struct Document {
	char *title;
	char *author;
	Part parts[];
};

struct Article {
	char *author;
	char *title;
	Section parts[];
};

struct Book {
	char *title;
	char *author;
	Chapter parts[];
};

void plagiarise(Document doc) {
	Part bogusPart = Section{"blah blah"};
	doc.author = "Sujit";
	addPart(doc.parts(bogusPart));
}


void printBookDetails(Book book) {
	print(book.title);
	print(book.author);
	for chapter in book.parts {
		printChapter(chapter);
	}
}

void printChapter(Part chapter) {
	print((Chapter)chapter.content;
	print((Chapter)chapter.refs;
}
 
int main() {
	Book b;
	plagiarise(b);
	printBookDetails(b);
}
