(* To illustrate dynamic programming through Fibonacci number *)

let rec fibonacci1 n =
  match n with
    0 | 1 -> 1
  |     _ -> (fibonacci1 (n - 1)) + (fibonacci1 (n - 2))
































(* Fibonacci with higher order generic memoise function *)
let memoise f =
  let table = ref []
  in
  let rec find tab n =
    match tab with
    | []           -> 
            let v = (f n)
            in
            table := (n, v) :: !table;
            v
    | (n', v) :: t -> 
            if n' = n then v else (find t n)
  in
  fun n -> find !table n
     
let fibonacci2 = memoise fibonacci1
























(* fibonacci with non-generic memoisation *)
let fibonacci3 n = 
  let table = ref [] in
  let rec fib3 n =
   let rec f3 n' =
      match n' with
        0 | 1 -> 1
      | _     -> (fib3 (n' - 1)) + (fib3 (n' - 2))
    in
    let rec find tab n'' =
      match tab with
      | []           -> 
              let v = (f3 n'')
              in
              table := (n'', v) :: !table;
              v
      | (n''', v) :: t -> 
              if n''' = n'' then v else (find t n'')
    in
    find !table n
  in
  fib3 n
