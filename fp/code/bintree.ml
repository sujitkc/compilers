(* To illustrate recursive definitions in unions *)
type bintree =
    Leaf
  | Node of int * bintree * bintree

let createATree () =
  let n1 = Node (1, Leaf, Leaf) in
  let n2 = Node (2, n1, Leaf)
  and n4 = Node (4, Leaf, Leaf)
  and n6 = Node (6, Leaf, Leaf) in
  let n5 = Node (5, n4, n6) in
  let n3 = Node (3, n2, n5)
  in
  n3

let rec find tree v =
  match tree with
    Leaf                      -> false
  | Node (x, _, _) when v = x -> true
  | Node (x, l, r)            -> if (v > x) then (find l v) else (find r v)




































let rec insert tree v =
  match tree with
    Leaf -> Node (v, Leaf, Leaf)
  | Node (x, _, _) when x = v -> tree
  | Node (x, l, r) -> 
    if x > v then Node (x, (insert l v), r)
    else          Node (x, l ,(insert r v))
