type student = {
  name        : string;
  mutable age : int;      (* this is a mutable field *)
  rollNumber  : int
}

let s1 = {
  name       = "Sujit";
  age        = 38;
  rollNumber = 1252
}
