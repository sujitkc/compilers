(* To illustrate how functions returned from functions is useful abstraction mechanism *)

let derivative f =
  let d x =
    let delta = 0.001
    in
    (f (x +. delta) -. f x) /. delta
  in
  d

let dderivative f =
  derivative (derivative f)
