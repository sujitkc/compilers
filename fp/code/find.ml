let find1 lst v =    
    let rec innerFind lst v i =
      match lst with 
        [] -> -1
      | h :: t ->
        if h = v then i
        else (innerFind t v (i + 1))
  in 
  (innerFind lst v 0)
;;


let find lst v =    
    let rec innerFind lst v i =
      match lst with 
        [] -> raise Not_found
      | h :: t ->
        if h = v then i
        else (innerFind t v (i + 1))
  in 
  (innerFind lst v 0)
;;

