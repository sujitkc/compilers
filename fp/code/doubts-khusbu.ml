(*
	Doubt 1 - Prime
*)

(* Khusbu's implementation *)
let rec trynext x n = 
  if (n mod x = 0) then false
  else
    trynext (x + 1) n
;;

let prime n =
  if (trynext 2 n) = false then "not prime" 
  else "prime"
;;

(* Sujit's implementation *)
let prime1 n =
  let root = int_of_float (sqrt (float_of_int n)) in
  let rec trynext x n = 
    if      (n mod x = 0) then false
    else if (x > root)    then true (* added this case *)
    else                       trynext (x + 1) n
  in
  if (trynext 2 n) = false then "not prime" 
  else "prime"
;;

(*
	Doubt 2 - Remove duplicates
*)
(* Khusbu's implementation *)
let rec revdup lst=
  if lst=[] then []
  else if (List.hd lst)=(List.hd (List.tl lst)) then (* This case prematurely assumes that the list length >= 2. You are not handling the case: length(lst) = 1. *)
    revdup (List.tl lst) (* Removed typo from 'let' to 'lst'. *)
  else
    revdup ([List.hd lst])@(List.tl lst) (* This also doesn't seem OK. It should be: 
    List.hd lst :: (revdup (List.tl lst)) *)
;;

(* Sujit's implementation *)
(*
Examples :
  revdup1 [1; 1; 2; 3; 4; 4; 5] -> [1; 2; 3; 4; 5]
  revdup1 ["Sujit"; "Kumar"; "Kumar"; "Chakrabarti"] = ["Sujit"; "Kumar"; "Chakrabarti"]
*)
let rec revdup1 lst =
  match lst with
    []                     -> []
  | h :: []                -> lst
  | f :: s :: t when f = s -> (revdup1 (s :: t))
  | h :: t                 -> h :: (revdup1 t)
;;
