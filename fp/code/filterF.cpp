#include <iostream>
#include <vector>

using namespace std;

bool isOdd(int n)  { return ((n % 2) == 1); }
bool isEven(int n) { return ((n % 2) == 0); }
bool lt3(int n)     { return n < 3;          }

vector<int> filterF(bool(*f)(int), vector<int> array) {
	vector<int> result;
	for(unsigned int i = 0; i < array.size(); i++) {
		if(f(array[i])) {
			result.push_back(array[i]);
		}
	}
	return result;
}

void printIntVector(vector<int> v) {
	cout << "[";
	for(unsigned int i = 0; i < v.size(); i++) {
		cout << v[i] << ", ";
	}
	cout << "]" << endl;
}

int main() {
	int inp[] = {1, 2, 3, 4, 5};
	vector<int> v(inp, inp + 5); 

	vector<int> r1 = filterF(isOdd, v);
	vector<int> r2 = filterF(isEven, v);

	printIntVector(r1);
	printIntVector(r2);

	return 0;
}
