(* To illustrate use of higher order function and polymorphic types *)
let rec merge f l1 l2 =
  match (l1, l2) with 
    ([], _) -> l2
  | (_, []) -> l1  
  | (h1 :: t1, h2 :: t2) ->
    if (f h1 h2) then (h1 :: (merge f t1 l2))
	else (h2 :: (merge f l1 t2))

let l1 = [1; 11; 21] and l2 = [5; 25; 30] and f x y = x < y;;
let l3 = [1.; 11.; 21.] and l4 = [5.; 25.; 30.] and g x y = x < y;;
