(* To illustrate anonymous binding *)
let rec getSeconds lot = 
  if lot = [] then []
  else let (_, s) = List.hd lot in
    s :: (getSeconds (List.tl lot));;
