float derivative(float(*f)(float), float x) {
  float dx = 0.00001;
  return (*f)(x + dx) - (*f)(x) / dx;
}
