(* To illustrate higher order functions using filter example *)

let rec filter f lst =
  match lst with
    [] -> []
  | h :: t ->
      if (f h) then (h :: (filter f t))
      else (filter f t)
