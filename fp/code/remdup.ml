(* To illustrate the use of closure through removing duplicates from a list *)

let rec removeDuplicates1 lst =
  match lst with
    [] -> []
  | h :: t ->
    if (List.mem h t) then
      (removeDuplicates t)
    else
      (h :: (removeDuplicates t))

let rec removeDuplicates2 lst =
  match lst with
    [] -> []
  | h :: t ->
    let t' -> removeDuplicates2 t
    in let check 
