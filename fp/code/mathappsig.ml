module type MyMath = sig
  val add :      int -> int -> int
  val subtract : int -> int -> int
end

module MyMathImpl : MyMath = struct
  let inc x = x + 1
  let dec x = x - 1
  let rec add x y = if y = 0 then x else (add (inc x) (dec y))
  let rec subtract x y = if y = 0 then x else (subtract (dec x) (dec y))
end

module Client = struct
  let clientFunction () =
    let a = 20 and b = 10 and c = 15 in
    (MyMathImpl.subtract (MyMathImpl.add a b) c)
end

module Driver = struct
let main () =
  print_int (Client.clientFunction ())
end
