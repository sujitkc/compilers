(* To illustrate the effect of too many recursions in recursive functions *)

(* Utility list creation functions *)
let rec range l h = if l > h then [] else l :: (range (l + 1) h)
let rev_range l h = List.rev (range l h)


































let rec max lst =
  match lst with
    []      -> raise Not_found
  | h :: [] -> h
  | h :: t  -> let maxt = (max t) in if h > (maxt) then h else (maxt)



































(*
let rec max2 lst =
  match lst with
    []      -> raise Not_found
  | h :: [] -> h
  | h :: t  -> let maxt = (max2 t) in if h > maxt then h else maxt
*)
