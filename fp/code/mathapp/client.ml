let clientFunction () =
  let a = 20 and b = 10 and c = 15 in
  (MyMath.subtract (MyMath.add a b) c)
