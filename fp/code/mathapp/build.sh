#!/bin/bash
files=( "myMath" "client" "driver" )
for file in ${files[@]}; do
	echo "compiling ${file}.ml"
	ocamlc -c  ${file}.ml
done
obj=""
for file in ${files[@]}; do
	obj="${obj} ${file}".cmo
done
ocamlc -o mathapp ${obj}
