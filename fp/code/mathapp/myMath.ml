let inc x = x + 1
let dec x = x - 1

let rec add x y = if y = 0 then x else (add (inc x) (dec y))
let rec subtract x y = if y = 0 then x else (subtract (dec x) (dec y))

