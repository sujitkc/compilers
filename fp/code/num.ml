(* To illustrate the definition of functions using pattern matching over unions *)

type number =
  Integer of int
  | Rational of int * int

let rec add n1 n2 =
  match (n1, n2) with
    (Integer i1, Integer i2)             -> Integer (i1 + i2)
  | Rational (n1, d1), Rational (n2, d2) -> Rational ((n1 * d2 + n2 * d1), (d1 * d2))
  | Rational (_, _), Integer i           -> add n1 (Rational (i, 1))
  | Integer i, Rational (n, d)           -> add n2 n1
