module MyMath = struct
let add x y = x + y
let subtract x y = x - y
end

module Client = struct
let clientFunction () =
  let a = 20 and b = 10 and c = 15 in
  (MyMath.subtract (MyMath.add a b) c)
end

module Driver = struct
let main () =
  print_int (Client.clientFunction ())
end

let _ = Driver.main ()
