class point (x_init,y_init) =
object
  val mutable x = x_init
  val mutable y = y_init
  method get_x = x
  method get_y = y
  method moveto (a,b) = x <- a ; y <- b
  method rmoveto (dx,dy) = x <- x + dx ; y <- y + dy
  method distance = sqrt (float(x*x + y*y))
  method to_string () =
    "( " ^ (string_of_int x) ^ ", " ^ (string_of_int y) ^ ")"
end

(* To illustrate containment *)
class polygon n =
object
  val mutable index = 0
  val points = Array.create n (new point(0,0))
  method add p =
    try points.(index) <- p ; index <- index + 1
      with Invalid_argument("Array.set")
    -> failwith ("polygon.add:index =" ^ (string_of_int index))
  method remove () = if (index > 0) then index <- index - 1
  method to_string () =
    let s = ref "["
    in
    for i = 0 to index - 1 do s := !s ^ " " ^ points.(i)#to_string () done ;
    (!s) ^ "]"
end

(* To illustrate inheritance *)
class coloured_point (x,y) c =
object
  inherit point (x,y)
  val mutable c = c
  method get color = c
  method set color nc = c <- nc
  method to_string () = "( " ^ (string_of_int x) ^
    ", " ^ (string_of_int y) ^ ")" ^
    " [" ^ c ^ "] "
end
