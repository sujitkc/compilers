type complex = {
  re : float;
  im : float
}

let c1 = {
  re = 1.;
  im = 2.3
}
