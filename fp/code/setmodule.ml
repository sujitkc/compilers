module SetA = struct
  type 'a set = NullSet | NonEmptySet of 'a * 'a set

  let makeset () = NullSet

  let rec setmem s e =
    match s with
      NullSet -> false
    | NonEmptySet(h, t) -> if (h = e) then true else (setmem t e)

  let add s e =
    match s with
      NullSet           -> NonEmptySet(e, NullSet)
    | NonEmptySet(h, t) -> if (setmem s e) then s else NonEmptySet(e, s)

  let rec intersect s1 s2 =
    match s1 with
      NullSet           -> NullSet
    | NonEmptySet(h, t) ->
        if (setmem s2 h) then NonEmptySet(h, (intersect t s2))
        else (intersect t s2)
end



(*
module SetB = struct
type 'a set = NullSet | NonEmptySet of 'a * 'a set

let makeset () = NullSet

let rec setmem s e =
  match s with
    NullSet -> false
  | NonEmptySet(h, t) -> if (h = e) then true else (setmem t e)

let add s e =
  match s with
    NullSet           -> NonEmptySet(e, NullSet)
  | NonEmptySet(h, t) -> if (setmem s e) then s else NonEmptySet(e, s)

let rec intersect s1 s2 =
  match s1 with
    NullSet           -> NullSet
  | NonEmptySet(h, t) ->
      if (setmem s2 h) then NonEmptySet(h, (intersect t s2))
      else (intersect t s2)
end
*)

let main () =
  let s = SetA.makeset ()
  in
  SetA.add (SetA.add (SetA.add s 1) 2) 3
