(* To illustrate tail-recursion through factorial example *)

(* Non tail recursive *)
let rec factorial1 n =
  if n = 0 then
    1
  else
    n * (factorial1 (n - 1))
































(* Tail recursive *)
let factorial2 n =
  let rec iter n fact =
    if n = 0 then
      fact
    else
      iter (n - 1) (n * fact)
  in
  iter n 1
