class virtual shape =
object
  method virtual area : unit -> int
end

class square s =
object inherit shape
  val side = s
  method area () = side * side
end
