(* To illustrate exception raising *)

let rec find dict v =
  match dict with
    [] -> raise Not_found
  | (key, value) :: t -> if key = v then value else (find t v)
